package drDolittle;

public interface Walking {
	public static double GRAVITY_CONSTANT = 9.8;
	public void walk();
}
