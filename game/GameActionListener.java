package game;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameActionListener implements ActionListener {
	GameFrame frame;
	
	GameActionListener(GameFrame f) {
		frame = f;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		frame.textArea.setVisible(false);
		frame.button.setVisible(false);
		frame.label.setText("Започваме!");
	}
}
