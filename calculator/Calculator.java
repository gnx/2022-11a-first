package calculator;

public class Calculator {
	public static int add(int a, int b) {
		return a + b;
	}
	
	public static String add(double a, double b) {
		return String.format("%.2f", a + b);
	}
}
