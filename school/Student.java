package school;

public class Student extends Person {
	public static boolean hasTextbooks = true;
	
	public Student(String n, int a) {
		super(n, a);
	}
	
	void print() {
		super.print();
		System.out.println("и съм ученик");
	}
	
	@Override
	public String toString() {
		return "име: " + name + "\nгодини: " + age;
	}
}
