package school;

public class Person extends Entity {
	protected String name;
	
	Person(String n, int a) {
		super(a);
		setName(n);
	}
	
	void print() {
		super.print();
		System.out.println("Казвам се " + getName() + ".");
	}
	
	void print(String greeting) {
		print();
		System.out.println("Казвам се " + getName());
	}
	
	void print(int studentClass) {
		print();
		System.out.println("и съм от " + studentClass + " клас.");
	}
	
	void print(Integer age) {
		print();
		System.out.println("и съм на " + age + " години.");
	}
	
	void print(int studentClass, String nationality) {
		print();
		System.out.println("и съм от " + studentClass + " клас");
		System.out.println("и съм " + nationality);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
